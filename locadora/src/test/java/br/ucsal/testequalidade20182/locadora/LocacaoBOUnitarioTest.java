package br.ucsal.testequalidade20182.locadora;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import br.ucsal.testequalidade20182.locadora.business.LocacaoBO;
import br.ucsal.testequalidade20182.locadora.dominio.Cliente;
import br.ucsal.testequalidade20182.locadora.dominio.Locacao;
import br.ucsal.testequalidade20182.locadora.dominio.Modelo;
import br.ucsal.testequalidade20182.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20182.locadora.persistence.ClienteDAO;
import br.ucsal.testequalidade20182.locadora.persistence.LocacaoDAO;
import br.ucsal.testequalidade20182.locadora.persistence.VeiculoDAO;

@RunWith(PowerMockRunner.class)
@PrepareForTest({LocacaoBO.class, VeiculoDAO.class, ClienteDAO.class, LocacaoDAO.class})
public class LocacaoBOUnitarioTest {

	/**
	 * Locar, para um cliente cadastrado, um veículo disponível. Método: public
	 * static Integer locarVeiculos(String cpfCliente, List<String> placas, Date
	 * dataLocacao, Integer quantidadeDiasLocacao) throws
	 //* ClienteNaoEncontradoException, VeiculoNaoEncontradoException,
	 * VeiculoNaoDisponivelException, CampoObrigatorioNaoInformado {
	 *
	 * Observações: lembre-se de mocar os métodos necessários nas classes
	 * ClienteDAO, VeiculoDAO e LocacaoDAO.
	 * 
	 * @throws Exception
	 */

	
	@Test
	public void locarClienteCadastradoUmVeiculoDisponivel() throws Exception {
		String cpf = "851.402.429-10";
		int quantidadeDias = 3;
		List <String> placas = Arrays.asList("placa1","placa2");
		Date data = new Date();
		String nome = "Haroldo";
		String telefone = "71988944612";
		Modelo modelo = new Modelo("Gurgel");
		
		//Veiculo
		Veiculo veiculo1 = new Veiculo(placas.get(0), 2018, modelo, 43d);
		Veiculo veiculo2 = new Veiculo(placas.get(1), 2019, modelo, 46d);
		PowerMockito.mockStatic(VeiculoDAO.class); 
		PowerMockito.when(VeiculoDAO.obterPorPlaca(placas.get(0))).thenReturn(veiculo1);
		PowerMockito.when(VeiculoDAO.obterPorPlaca(placas.get(1))).thenReturn(veiculo2);

		
		//cliente
		Cliente cliente = new Cliente(cpf, nome, telefone);
		PowerMockito.mockStatic(ClienteDAO.class);
		PowerMockito.when(ClienteDAO.obterPorCpf(cpf)).thenReturn(cliente);
		
		//locacao
		List<Veiculo> veiculos  = Arrays.asList(veiculo1);
		Locacao locacao = new Locacao(cliente, veiculos, data, quantidadeDias);	
		Whitebox.setInternalState(locacao, "numeroContrato", 399); 
		PowerMockito.whenNew(Locacao.class).withAnyArguments().thenReturn(locacao); 

		
		//PowerMockito.mockStatic(LocacaoDAO.class);
		
		int atual = LocacaoBO.locarVeiculos(cpf, placas, data, quantidadeDias);
		Assert.assertEquals(399, atual);
	}


}
